<?php
$labels = array(
    'name' => _x( 'Modalidades', 'taxonomy general name' ),
    'singular_name' => _x( 'Modalidade', 'taxonomy singular name' ),
    'search_items' =>  __( 'Buscar Modalidades' ),
    'all_items' => __( 'Todas as Modalidades' ),
    'parent_item' => __( 'Modalidade Pai ' ),
    'parent_item_colon' => __( 'Modalidade Pai:' ),
    'edit_item' => __( 'Editar Modalidade' ), 
    'update_item' => __( 'Atualizar Modalidade' ),
    'add_new_item' => __( 'Adicionar nova Modalidade' ),
    'new_item_name' => __( 'Nome da nova Modalidade' ),
    'menu_name' => __( 'Modalidades' ),
);    
// Now register the taxonomy
register_taxonomy('modalidades',array('movies'), array(
    'hierarchical' => true,
    'labels' => $labels,
    'show_ui' => true,
    'show_admin_column' => true,
    'query_var' => true,
    'rewrite' => array( 'slug' => 'modalidades' ),
));