<?php
function addPostMetaBoxes(){
	add_meta_box('metaBoxFeatured','Destaque','metaBoxFeatured', array('post','page'),'side','high');
	function metaBoxFeatured($post){
		include(get_template_directory().'/inc/customMetaPosts/metaBoxFeatured.php');	
	}
}
add_action( 'add_meta_boxes', 'addPostMetaBoxes' );
// Save
function saveCustomMetaPosts($post_id){
	include(get_template_directory().'/inc/customMetaPosts/savePost.php');

}
add_action('save_post', 'saveCustomMetaPosts');