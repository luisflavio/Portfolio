<?php
class BuildHTML{
	private $layout,$directoryHTML,$vars;
	public function __construct($args=array()){
		//Default Values
		$this->vars=array();
		$this->directoryHTML=get_template_directory().'/inc/html/';
		$this->layout='';
		//Setting Layout
		if(isset($args['layout'])) $this->layout=$this->directoryHTML.$args['layout'];
		if(is_file($this->layout))$this->layout=file_get_contents($this->layout);
	}
	public function setVars($vars,$values=null){
		if(is_array($vars))foreach ($vars as $key => $value) {$this->vars[$key]=$value;}
		else $this->vars[$vars]=$values;
	}
	private function processVars(){
		foreach($this->vars as $key => $value){
			if(is_array($value)){

			}else{
				$this->layout=str_replace('{'.$key.'}',$value,$this->layout);
			}
		}
	}
	private function wpVars(){
		$vars['siteUrl']=get_site_url();
		$vars['themeUrl']=get_template_directory_uri();
		foreach($vars as $k=>$v){
			$this->layout=str_replace('{'.$k.'}',$v,$this->layout);
		}
	}
	public function build(){
		$this->processVars();
		$this->wpVars();
		return $this->layout;
	}
	public function print(){
		echo $this->build();
	}

}