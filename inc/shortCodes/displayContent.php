<?php
if(!is_array($atts))$atts=array();
if(!isset($atts['layout']))$atts['layout']='default';
if(!isset($atts['css-class']))$atts['classText']=' ';
else $atts['classText']=$atts['css-class'];
$layout['box']='shortCodes/displayContent.'.$atts['layout'].'.box.html';
$vars['content']=do_shortcode($content);
$vars['classText']=$atts['classText'];
$BuildHTML=new BuildHTML(array('layout'=>$layout['box']));
$BuildHTML->setVars($vars);
$return=$BuildHTML->build();
