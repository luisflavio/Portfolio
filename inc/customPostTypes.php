<?php
//Adicionando Custom ost Type dos Planos da Vivo
function createPostType() {
	$labels = array(
        'name'                => _x( 'Movies', 'Post Type General Name', 'lrDefaultTheme' ),
        'singular_name'       => _x( 'Movie', 'Post Type Singular Name', 'lrDefaultTheme' ),
        'menu_name'           => __( 'Movies', 'lrDefaultTheme' ),
        'parent_item_colon'   => __( 'Parent Movie', 'lrDefaultTheme' ),
        'all_items'           => __( 'All Movies', 'lrDefaultTheme' ),
        'view_item'           => __( 'View Movie', 'lrDefaultTheme' ),
        'add_new_item'        => __( 'Add New Movie', 'lrDefaultTheme' ),
        'add_new'             => __( 'Add New', 'lrDefaultTheme' ),
        'edit_item'           => __( 'Edit Movie', 'lrDefaultTheme' ),
        'update_item'         => __( 'Update Movie', 'lrDefaultTheme' ),
        'search_items'        => __( 'Search Movie', 'lrDefaultTheme' ),
        'not_found'           => __( 'Not Found', 'lrDefaultTheme' ),
        'not_found_in_trash'  => __( 'Not found in Trash', 'lrDefaultTheme' ),
    );
     
// Set other options for Custom Post Type
     
    $args = array(
        'label'               => __( 'movies', 'lrDefaultTheme' ),
        'description'         => __( 'Movie news and reviews', 'lrDefaultTheme' ),
        'labels'              => $labels,
        // Features this CPT supports in Post Editor
        'supports'            => array( 'title', 'editor', 'excerpt', 'author', 'thumbnail', 'comments', 'revisions', 'custom-fields', ),
        // You can associate this CPT with a taxonomy or custom taxonomy. 
        'taxonomies'          => array( 'genres' ),
        /* A hierarchical CPT is like Pages and can have
        * Parent and child items. A non-hierarchical CPT
        * is like Posts.
        */ 
        'hierarchical'        => false,
        'public'              => true,
        'show_ui'             => true,
        'show_in_menu'        => true,
        'show_in_nav_menus'   => true,
        'show_in_admin_bar'   => true,
        'menu_position'       => 20,
        'menu_icon'			  => 'dashicons-media-spreadsheet',
        'can_export'          => true,
        'has_archive'         => true,
        'exclude_from_search' => false,
        'publicly_queryable'  => true,
        'capability_type'     => 'page',
    );
     
    // Registering your Custom Post Type
    register_post_type( 'movies', $args );
}
add_action( 'init', 'createPostType' );

