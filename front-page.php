<?php
get_header();
$BuildHTML=new BuildHTML(array('layout'=>'front-page.html'));
while ( have_posts() ) : the_post();
	$vars=array(
		'pageTitle'=>get_the_title(),
		'pageContent'=>do_shortcode( get_the_content(),$ignore_html = false),
	);
endwhile;
$BuildHTML->setVars($vars);
//$BuildHTML->setVars('contentDefault','Conteudo Teste');
//$BuildHTML->setVars(array('contentDefault2'=>'Conteudo Teste2'));
$BuildHTML->print();
get_footer();