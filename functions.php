<?php

//Adicionando Classes
include(get_template_directory().'/inc/classes/BuildHTML.php');
//Definindo tempo
date_default_timezone_set('America/Sao_Paulo');
/*** Adicionado suorte a resumo e thunbnails em paginas e posts  ***/
add_action('init', 'wp_posts_suports');
function wp_posts_suports() {
    add_theme_support( 'post-thumbnails', array( 'post','page' ) );  
    add_post_type_support( 'page', array('excerpt') );
}
//Ocultando Admin Bar
show_admin_bar(false);
//Setando o Wp Mail para envio HTML
add_filter('wp_mail_content_type','set_content_type');
function set_content_type($content_type){return 'text/html';}
//Registrando Styles e Js
function add_theme_scripts(){

	wp_enqueue_style(
		'bootstrap-4.0.0',
		'https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css'
	);
	wp_enqueue_style( 'css-all', get_template_directory_uri() . '/assets/css/style.css', array(), '1.1', 'all');
	wp_enqueue_style( 'css-desktop', get_template_directory_uri() . '/assets/css/style.desktop.css', array(), '1.1', 'min-width: 993px');
	wp_enqueue_script(
		'jquery-3.3.1',
		'https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js'
	);
	
	wp_enqueue_script(
		'tether-1.4.3',
	 	'https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.3/js/tether.min.js'
	);
	wp_enqueue_script(
		'bootstrap-4.0.0',
	 	'https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0-alpha.4/js/bootstrap.min.js'
	);
	wp_enqueue_script( 'script-all', get_template_directory_uri() . '/assets/js/script.js');
}
add_action( 'wp_enqueue_scripts', 'add_theme_scripts' );

/*** Adicionado menus ***/
function registrar_menu(){
	register_nav_menus(
		array(
			'menuPrincipal' => 'Menu Principal',
		)
	);
}
add_action( 'init', 'registrar_menu' );
//Adicionando CustomPostTypes
include(get_template_directory().'/inc/customPostTypes.php');
//Adicionando CustomPostTypes
include(get_template_directory().'/inc/customMetaPosts.php');
//Adicionando shortCodes
include(get_template_directory().'/inc/shortCodes.php');
/*** Incluindo Taxonomias ***/
include(get_template_directory().'/inc/taxonomies.php');
/*** Incluindo requisicoes ajax ***/
include(get_template_directory().'/inc/ajax.php');
